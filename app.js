var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var connect = require('connect');
var flash = require("connect-flash");
var session = require('express-session');
var methodOverrive = require('express-method-override');
var passport = require("passport");

//var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var login = require('./routes/login');
var index = require('./routes/index');
var dashboard = require('./routes/dashboard');
var users = require('./routes/users');
var categories = require('./routes/categories');
var colors = require('./routes/colors');
var projects = require('./routes/projects');
var schedule = require('./routes/schedule');
var gantt = require('./routes/gantt');
var gantt_template = require('./routes/gantt_template');
var grid = require('./routes/grid');

var app = express();

var dataprovider = require('./dataprovider/dataprovider');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));
app.use('/scheduler',  express.static(__dirname + '/scheduler'));
app.use('/gantt',  express.static(__dirname + '/gantt'));
app.use('/gantt_template',  express.static(__dirname + '/gantt_template'));
app.use('/grid',  express.static(__dirname + '/grid'));

app.use( methodOverrive('_method') );

app.use(passport.initialize());

app.use(flash());
//cookieを使う
app.use(session(
  { secret: 'oitecsys' ,
    cookie: {
        httpOnly: false,
        //maxAge: new Date(Date.now() + 60 * 60 * 1000000)        
    }  
  }
)); 

// passport が ユーザー情報をシリアライズすると呼び出されます
passport.serializeUser(function (user, done) {
  console.log('serializeUser');
  done(null, user);
});

// passport が ユーザー情報をデシリアライズすると呼び出されます
passport.deserializeUser(function (id, done) {
  dataprovider.getUserById(id).then(function (user){
      done(null, user);
  });
  console.log('deserializeUser');
  //done(null, id);
});


app.use('/', index);
app.use('/login', login);
app.use('/dashboard', dashboard);
app.use('/users', users);
app.use('/categories', categories);
app.use('/colors', colors);
app.use('/projects', projects);
app.use('/schedule', schedule);
app.use('/gantt', gantt);
app.use('/gantt_template', gantt_template);
app.use('/grid', grid);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;

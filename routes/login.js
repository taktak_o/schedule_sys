var express = require('express');
var connect = require('connect');
var bodyParser = require('body-parser');
var passport = require("passport");
var flash = require("connect-flash");
var LocalStrategy = require("passport-local").Strategy;
var session = require('express-session');
var cipherprovider = require('../public/modules/cipherprovider');
var checkAuth = require('./checkAuth');

var router = express.Router();


require('date-utils').language('jp');

var dataprovider = require('../dataprovider/dataprovider');



passport.use(new LocalStrategy(
  {
    usernameField: 'uid', 
    passwordField: 'password',
    passReqToCallBack: true
  },
  function(username, password, done) {
    dataprovider.getUser( username ).then(function (user) {      
      if (!user) {
        console.log('user.id not match:' + username);        
        return done(null, false, { message: 'ユーザーIDが正しくありません。' });        
      }
      var codePassword = cipherprovider.getCipher(password);
      if (user.password != codePassword) {        
        console.log('password not match user.password:' + user.password + "password: " + codePassword);
        return done(null, false, { message: 'パスワードが正しくありません。' });
      }
      console.log('user.password:' + user.password + "password: " + codePassword);
      return done(null, user);
    });
  }
));




router.get('/', function(req, res) {
    //log
    //res.redirect(req.session.passport.originalUrl);
    if(req.session.passport !== undefined ){
      return res.redirect("/dashboard");
    }

    console.log('user login :' );
    res.render('./login/login',{ message: req.flash('error'), title: 'ログイン' });
  });

router.post('/', 

  function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
      if (err) { return next(err); }
      //認証に失敗したとき
      if (!user) {
        req.flash('error',info.message);
        return res.redirect('/login');
      }

      req.logIn(user, function(err) {        
        if (err) {
          return next(err); 
        }        
        var redirectUrl
        if(!req.session.originalUrl){
          //要求があったURLが保存されてないとき
          redirectUrl = "/dashboard";
        } else {
          //要求があったURLが保存されてるとき
          redirectUrl = req.session.originalUrl;
        }
        req.session.originalUrl = null;
        //ここのリダイレクト先でセッションが変わってしまう
        return res.redirect( redirectUrl );
      });
    })(req, res, next);
  })

module.exports = router;
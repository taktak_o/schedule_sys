var express = require('express');
var connect = require('connect');
var session = require('express-session');
var cipherprovider = require('../public/modules/cipherprovider');
var router = express.Router();
var checkAuth = require('./checkAuth');

require('date-utils').language('jp');

var dataprovider = require('../dataprovider/dataprovider');



//プロジェクト一覧表示
router.get('/',  checkAuth.isAuthenticated,function(req, res, next) {
  console.log('projects show: ' + req.session.passport.user.uid);
  
  projects = function(){
    return dataprovider.getProjects(req.session.passport.user.uid);
  };
  console.log('projects : ' + projects);
  res.render('./projects/projects',{ projects , title: '案件一覧' });    
});

router.get('/addnew', checkAuth.isAuthenticated,function(req, res) {
  console.log('project addnew');
  var user_id = req.session.passport.user.id;
  var username = req.session.passport.user.name;
  var role = req.session.passport.user.role;

  Promise.all([
    dataprovider.getCategories(),
    dataprovider.getUsergrops(user_id),
    dataprovider.getProjectTemplate(),
    dataprovider.getCustomers(),
    dataprovider.getUsers(),
    dataprovider.getColors(),
  ]).then(function(onfulfilled){
    var categories = onfulfilled[0];
    var usergroups = onfulfilled[1];
    var project_templates = onfulfilled[2];
    var customers = onfulfilled[3];
    var users = onfulfilled[4];
    var colors = onfulfilled[5];

    var data = {
      categories,
      usergroups, 
      project_templates, 
      customers, 
      users, 
      colors, 
    };      

    res.render('./projects/addproject',{ username, role　, title:'新規案件作成', data: data });
  })
});

router.post('/addnew', checkAuth.isAuthenticated,function(req, res, next) {

  var projects = {
    pid: req.body.pid,
    color: req.body.color,
    category_id: req.body.category_id,
    categoryname: dataprovider.getCategorieById(req.body.category_id).categoryname,
    project_name: req.body.project_name,
    project_description: req.body.project_description,
    start_date: req.body.start_date,
    end_date: req.body.end_date,
    finish_date: req.body.finish_date,
    owner_id: req.body.owner_id,
    owner_name: '',
    usergroup_id: req.body.usergroup_id,
    usergroup_name: '',
    contract_amount: req.body.contract_amount,
    budget_total: req.body.budget_total,
    settlement_total: req.body.settlement_total,
    spreadsheetkey: req.body.spreadsheetkey,
    customer_id: req.body.customer_id || null,
    customer_name: '',
  };

  dataprovider.insertProjects(projects);
});

module.exports = router;
var express = require('express');
var router = express.Router();

var dataprovider = require('../dataprovider/dataprovider');
var checkAuth = require('./checkAuth');

/* GET home page. */

router.get('/', checkAuth.isAuthenticated,function(req, res, next) {

    /*
    console.log('schedule show: ' + req.user.uid);  
    var user_id = req.user.id;
    var username = req.user.name;
    var role = req.user.role;  
    var title = username + "さんのスケジュール";
    */

    console.log('schedule show: ' + req.session.passport.user.uid);  
    var user_id = req.session.passport.user.id;
    var username = req.session.passport.user.name;
    var role = req.session.passport.user.role;  
    var title = username + "さんのスケジュール";
    var task_list = [];


	//選択リスト用にデータを取得する
    Promise.all([
        dataprovider.getProjectTaskList(7),     //終了していないものだけ
        dataprovider.getColors()
      ]).then(function(onfulfilled){    
        var rows = onfulfilled[0];
        var colors = onfulfilled[1];
        var selcolors = [{key:"", label: "色を選択..."}];
        for(var i=0;i<colors.length;i++){
            selcolors.push({key: colors[i].color, label: colors[i].name});
            select_colors=JSON.stringify(selcolors);
        }

        for(var i=0;i<rows.length;i++){        
            //var task_list = [{key:"", label: "作業を選択..."}];
            task_list.push({key: rows[i].task_id, label: rows[i].project_title +" "+ rows[i].task_title});
            var tasks=JSON.stringify(task_list);
        };
        if(!tasks){tasks="[]"};
        res.render('../views/schedule/schedule', { title: title , role: role, tasks, select_colors });  
        });
});


router.get('/plan', checkAuth.isAuthenticated,function(req, res, next) {
    
        /*
        console.log('schedule show: ' + req.user.uid);  
        var user_id = req.user.id;
        var username = req.user.name;
        var role = req.user.role;  
        var title = username + "さんのスケジュール";
        */
    
        console.log('schedule show: ' + req.session.passport.user.uid);  
        var user_id = req.session.passport.user.id;
        var username = req.session.passport.user.name;
        var role = req.session.passport.user.role;  
        var title = username + "さんのスケジュール計画";
        var task_list = [];
    
    
        //選択リスト用にデータを取得する
        Promise.all([
            dataprovider.getProjectTaskList(7),     //終了していないものだけ
            dataprovider.getColors()
          ]).then(function(onfulfilled){    
            var rows = onfulfilled[0];
            var colors = onfulfilled[1];
            var selcolors = [{key:"", label: "色を選択..."}];
            for(var i=0;i<colors.length;i++){
                selcolors.push({key: colors[i].color, label: colors[i].name});
                select_colors=JSON.stringify(selcolors);
            }
    
            for(var i=0;i<rows.length;i++){        
                //var task_list = [{key:"", label: "作業を選択..."}];
                task_list.push({key: rows[i].task_id, label: rows[i].project_title +" "+ rows[i].task_title});
                var tasks=JSON.stringify(task_list);
            };
            if(!tasks){tasks="[]"};
            res.render('../views/schedule/scheduleP', { title: title , role: role, tasks, select_colors });  
            });
    });


router.get('/data',  checkAuth.isAuthenticated,function(req, res, next) {
  console.log('schedule data: ' + req.session.passport.user.uid);
  var user_id = req.session.passport.user.id;
  
  dataprovider.getEvents(user_id).then(function(eventlist){
    data = JSON.stringify(eventlist);    
		res.send(data);
  });
});


router.post('/data', function(req, res){
    var user_id = req.session.passport.user.id;
    var data = req.body;
    //get operation type
    var mode = data["!nativeeditor_status"];
    //get id of record
    var sid = data.id;
    var tid = sid;
  
    //remove properties which we do not want to save in DB
    delete data.id;
    delete data.gr_id;
    delete data["!nativeeditor_status"];

	function update_response(err, result){
		if (err)
			mode = "error";
		else if (mode == "inserted")
			tid = result.insertId;

            res.setHeader("Content-Type","text/xml");
            res.send("<data><action type='"+mode+"' sid='"+sid+"' tid='"+tid+"'/></data>");   
	}

	if (mode == "updated")
    dataprovider.updateEvents( sid, data, update_response);
	else if (mode == "inserted")
    dataprovider.insertEvents( data , user_id , update_response);
	else if (mode == "deleted")
    dataprovider.deleteEvents( sid, update_response);
	else
    res.send("Not supported operation");
    

    



});


module.exports = router;

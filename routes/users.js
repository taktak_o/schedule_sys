var express = require('express');
var connect = require('connect');
var session = require('express-session');
var flash = require("connect-flash");
var cipherprovider = require('../public/modules/cipherprovider');
var router = express.Router();

require('date-utils').language('jp');

var dataprovider = require('../dataprovider/dataprovider');

var User = function(){
  return {
      uid: '',
      name: '',
      role: '',
      email: '',
      password: '',
      createdate: '',
      editdate: '',
  }
};

function checkAuthentication(req, res, next){  
  console.log('checkAuthentication');
  if (req.session.passport !== undefined) {  // 認証済
    console.log('checkAuthentication: true');
      return next();
  }
  else {  // 認証されていない
      console.log('checkAuthentication: false');
      res.redirect('/login');  // ログイン画面に遷移
  }
}

//ユーザー一覧表示
router.get('/',  checkAuthentication,function(req, res, next) {
  console.log('users show: ' + req.session.passport.user.uid);
  var users;
  var role = req.session.passport.user.role;
  var sqlstring = "select id, uid, name from users" 
  dataprovider.getUsers().then( function(users){
      console.log(users);
      res.render('./users/users',{ users, role, title: 'ユーザー一覧' });    
  })    
});

//ユーザー追加フォーム　uidがユニークかどうか確認する
router.get('/addnew', function(req, res, next) {
  console.log('user addnew');
  var user = new User;
  var mode = 'add';
  var role = req.session.passport.user.role;
  var encodePassword = null;
  res.render('./users/user',{ user, mode, method: 'POST', role, title:'ユーザー追加', encodepassword: encodePassword , message: req.flash('error')});
});

//ユーザーデータ削除
router.post('/delete', function(req, res, next) {
  console.log('delete userdata'); 
  var user_id = req.body.id 
  dataprovider.deleteUser(user_id);
  var dialogcontent = {
    title: '',
    text: 'ユーザーデータを削除しました',
    footer: '<a href="./"><button class="btn btn-primary">閉じる</button></a>',
  };
  res.render('./dialog',{ d: dialogcontent });
});

//ユーザーデータ追加
router.post('/', function(req, res, next) {
  console.log('add userdata');
  var user = setUser(req);  
  dataprovider.insertUser(user);
  var dialogcontent = {
    title: '',
    text: req.body.name + '　さんのデータ追加をしました',
    footer: '<a href="/users"><button class="btn btn-primary">閉じる</button></a>',
  };    
  res.render('./dialog',{ d: dialogcontent });
  res.redirect('/users');
});

//ユーザーデータ更新
router.put('/', function(req, res, next) {
  console.log('update userdata');    
  var user = setUser(req);  
  var user_id = req.body.id  
  dataprovider.updateUser(user_id,user);

  var dialogcontent = {
    title: '',
    text: 'ユーザーデータを更新しました',
    footer: '<a href="/users"><button class="btn btn-primary">閉じる</button></a>',
  };
  
  res.render('./dialog',{ d: dialogcontent });
});

//ユーザー編集画面
router.get('/:id', function(req, res, next) {
  console.log('user editform');
  var mode = 'edit';
  var role = req.session.passport.user.role;

  dataprovider.getUserById( req.params.id ).then(function(user){
    var encodePassword = cipherprovider.getDeCipher(user['password']); 
    res.render('./users/user',{ user, mode, role, method: 'PUT', title:'ユーザー編集' , encodepassword: encodePassword } );
  });  
});

function setUser(req){
  var codePassword = cipherprovider.getCipher(req.body.password);
  var user = {
    uid: req.body.uid,
    name: req.body.name,
    email: req.body.email, 
    role: req.body.role,
    password: codePassword
  }
  return user;
}

module.exports = router;
var express = require('express');
var connect = require('connect');
var session = require('express-session');
var flash = require("connect-flash");
var cipherprovider = require('../public/modules/cipherprovider');
var router = express.Router();

require('date-utils').language('jp');

var dataprovider = require('../dataprovider/dataprovider');

var Category = function(){
    return {
        id: '',
        categoryName: '',
        createdate: '',
        editdate: '',
    }
  };

function checkAuthentication(req, res, next){  
  console.log('checkAuthentication');
  if (req.session.passport !== undefined) {  // 認証済
    console.log('checkAuthentication: true');
      return next();
  }
  else {  // 認証されていない
      console.log('checkAuthentication: false');
      res.redirect('/login');  // ログイン画面に遷移
  }
}

//カテゴリー一覧
router.get('/',  checkAuthentication,function(req, res, next) {
  console.log('categories show: ' );
  var categories;
  var role = req.session.passport.user.role;  
  dataprovider.getCategories().then( function(categories){
      console.log(categories);
      res.render('./categories/categories',{ categories, role, title: 'カテゴリー一覧' });    
  })
});

//カテゴリー追加フォーム
router.get('/addnew', function(req, res, next) {
  console.log('categories addnew');  
  var category = new Category;
  var role = req.session.passport.user.role;
  var mode = 'add';  
  res.render('./categories/category',{ category, mode, role, method: 'POST', title:'カテゴリー追加'});
});

//カテゴリー削除
router.post('/delete', function(req, res, next) {
  console.log('delete categories');
  var category_id = req.body.id 
  dataprovider.deleteCategory(category_id);
  var dialogcontent = {
    title: '',
    text: 'カテゴリーを削除しました',
    footer: '<a href="./"><button class="btn btn-primary">閉じる</button></a>',
  };
  res.render('./dialog',{ d: dialogcontent });
});

//カテゴリー追加
router.post('/', function(req, res, next) {
  console.log('add categories');
  var category = req.body.categoryName;
  dataprovider.insertCategory(category);
  var dialogcontent = {
    title: '',
    text: req.body.categoryName + '　を追加しました',
    footer: '<a href="/categories"><button class="btn btn-primary">閉じる</button></a>',
  };    
  res.render('./dialog',{ d: dialogcontent });
  res.redirect('/categories');
});

//カテゴリー更新
router.put('/', function(req, res, next) {
  console.log('update categories');    
  var category = req.body.categoryName;  
  var category_id = req.body.id;
  dataprovider.updateCategory(category_id,category);

  var dialogcontent = {
    title: '',
    text: 'カテゴリーを更新しました',
    footer: '<a href="/categories"><button class="btn btn-primary">閉じる</button></a>',
  };
  
  res.render('./dialog',{ d: dialogcontent });
});

//カテゴリー編集画面
router.get('/:id', function(req, res, next) {
  console.log('categories editform');
  var mode = 'edit';
  var role = req.session.passport.user.role;
  dataprovider.getCategoryById( req.params.id ).then(function(category){    
    res.render('./categories/category',{ category, mode, role, method: 'PUT', title:'カテゴリー編集'  } );
  });  
});


module.exports = router;
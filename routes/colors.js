var express = require('express');
var connect = require('connect');
var session = require('express-session');
var flash = require("connect-flash");
var cipherprovider = require('../public/modules/cipherprovider');
var router = express.Router();

require('date-utils').language('jp');

var dataprovider = require('../dataprovider/dataprovider');

var Color = function(){
    return {
        id: '',
        color: '',
        name: '',
        createdate: '',
        editdate: '',
    }
  };

function checkAuthentication(req, res, next){  
  console.log('checkAuthentication');
  if (req.session.passport !== undefined) {  // 認証済
    console.log('checkAuthentication: true');
      return next();
  }
  else {  // 認証されていない
      console.log('checkAuthentication: false');
      res.redirect('/login');  // ログイン画面に遷移
  }
}

//一覧
router.get('/',  checkAuthentication,function(req, res, next) {
  console.log('colors show: ' );
  var colors;
  var role = req.session.passport.user.role;  
  dataprovider.getColors().then( function(colors){
      console.log(colors);
      res.render('./colors/colors',{ colors, role, title: '色一覧' });    
  })
});

//追加フォーム
router.get('/addnew', function(req, res, next) {
  console.log('colors addnew');  
  var color = new Color;
  var role = req.session.passport.user.role;
  var mode = 'add';  
  res.render('./colors/color',{ color, mode, role, method: 'POST', title:'色追加'});
});

//削除
router.post('/delete', function(req, res, next) {
  console.log('delete colors');
  var color_id = req.body.id 
  dataprovider.deleteColor(color_id);
  var dialogcontent = {
    title: '',
    text: '色を削除しました',
    footer: '<a href="./"><button class="btn btn-primary">閉じる</button></a>',
  };
  res.render('./dialog',{ d: dialogcontent });
});

//追加
router.post('/', function(req, res, next) {
  console.log('add color');
  var color = new Color;
  color.name = req.body.name;
  color.color = req.body.color;

  dataprovider.insertColor(color);
  var dialogcontent = {
    title: '',
    text: req.body.name + '　を追加しました',
    footer: '<a href="/colors"><button class="btn btn-primary">閉じる</button></a>',
  };    
  res.render('./dialog',{ d: dialogcontent });
  res.redirect('/colors');
});

//更新
router.put('/', function(req, res, next) {
  console.log('update colors');
  var color = new Color;
  color.name = req.body.name;  
  color.color = req.body.color;
  var color_id = req.body.id;
  dataprovider.updateColor(color_id,color);

  var dialogcontent = {
    title: '',
    text: '色を更新しました',
    footer: '<a href="/colors"><button class="btn btn-primary">閉じる</button></a>',
  };
  
  res.render('./dialog',{ d: dialogcontent });
});

//編集画面
router.get('/:id', function(req, res, next) {
  console.log('colors editform');
  var mode = 'edit';
  var role = req.session.passport.user.role;
  dataprovider.getColorById( req.params.id ).then(function(color){    
    res.render('./colors/color',{ color, mode, role, method: 'PUT', title:'カテゴリー編集'  } );
  });  
});


module.exports = router;
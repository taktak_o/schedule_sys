var express = require('express');
var connect = require('connect');
var session = require('express-session');
var cipherprovider = require('../public/modules/cipherprovider');
var passport = require("passport");
var checkAuth = require('./checkAuth');
var router = express.Router();
require('date-utils').language('jp');
//require("date-format-lite");

var dataprovider = require('../dataprovider/dataprovider');

var projects = [{
    id: '',
    pid: '',
    color: '',
    category_id: '',
    categoryname: '',
    project_name: '',
    project_description: '',
    start_date: '',
    end_date: '',
    finish_date: '',
    owner_id: '',
    owner_name: '',
    usergroup_id: '',
    usergroup_name: '',
    contract_amount: '',
    budget_total: '',
    settlement_total: '',
    spreadsheetkey: '',
    customer_id: '',
    customer_name: '',
    createdate: '',
    editdate: '',   
  }]


router.get('/',  checkAuth.isAuthenticated,function(req, res, next) {
    var user_id = req.session.passport.user.id;
    var username = req.session.passport.user.name;
    var role = req.session.passport.user.role;
    var title = username + 'さんのダッシュボード';

  //選択リスト用にデータを取得する
  var min_date = new Date().date("YYYY/MM/DD");
  var max_date = new Date(max_date + 60*60*24*7*1000).date("YYYY/MM/DD");
  Promise.all([
    dataprovider.getProjectTaskList(),
    dataprovider.getEvents(user_id,new Date(),null )
  ]).then(function(onfulfilled){
    var projects = onfulfilled[0];
    var events = onfulfilled[1];

    projects.task_start_date = function(){if(!projects.task_start_date){return "";} else {return projects.task_start_date.date('YYYY-MM-DD')};}
    projects.task_end_dateA = function(){if(!projects.task_end_dateA){return "";} else {return projects.task_end_dateA.date('YYYY-MM-DD')};}

    res.render('./dashboard/dashboard',{title: title, role: role, projects, events});
  });   

});

module.exports = router;

var express = require('express');
var app = require('../app');
var common = require('../bin/common');
var dataprovider = require('../dataprovider/dataprovider');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var companyname = common.getCompanyName;
  res.render('index', { companyname: companyname });
});

//ログアウト
router.get('/logout', function(req, res, next) {
  console.log('logout');
  req.session.destroy();
  
	req.logout();
	res.redirect('/');
});


router.get('/holidays', function(req, res, next) {
  
  dataprovider.Holidays("2017/1/1","2020/12/31").then(function(onfulfilled){    
    res.send(onfulfilled);
  });
});



module.exports = router;

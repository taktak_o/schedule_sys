var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var session = require('express-session');
var path = require('path');
var checkAuth = require('./checkAuth');
var dataprovider = require('../dataprovider/dataprovider');
var htmlgenerator = require('../dataprovider/htmlgenerator');

require("date-format-lite");


router.get('/', checkAuth.isAuthenticated,function(req, res, next) {
  console.log('gantt show: ' + req.session.passport.user.uid);  
  var user_id = req.session.passport.user.id;
  var username = req.session.passport.user.name;
  var role = req.session.passport.user.role;  
  var title = username + "さんのガントチャート";

	//選択リスト用にデータを取得する
  Promise.all([
    dataprovider.getCategories(),
		dataprovider.getColors(),
		dataprovider.getUsers(),
		dataprovider.getProjectList(),
		dataprovider.getProjectListTemplates(),
  ]).then(function(onfulfilled){

    var categories = onfulfilled[0];
    var colors = onfulfilled[1];
		var users = onfulfilled[2];
		var projectlist = onfulfilled[3];
		var projectlistTemplate = onfulfilled[4];
		
		//カテゴリー選択リスト
		var selcategories = [{key:"", label: "カテゴリーを選択..."}];
		for(var i=0;i<categories.length;i++){
			selcategories.push({key: categories[i].id, label: categories[i].categoryname});			
		}
		select_categories=JSON.stringify(selcategories);
		
		//色選択リスト
		var selcolors = [{key:"", label: "色を選択..."}];
		for(var i=0;i<colors.length;i++){
			selcolors.push({key: colors[i].color, label: colors[i].name});			
		}
		select_colors=JSON.stringify(selcolors);

		//ユーザー選択リスト
		var selusers = [];
		for(var i=0;i<users.length;i++){
			selusers.push({key: users[i].id, label: users[i].name});
		}
		select_users=JSON.stringify(selusers);

		//プロジェクト選択リスト
		var project_list = [];
		for(var i=0;i<projectlist.length;i++){			
			project_list.push({value: projectlist[i].id , text: projectlist[i].text});
		}
		project_list=JSON.stringify(project_list);

		//プロジェクトテンプレート選択リスト
		var select_template = [{key:"", label: "テンプレートなし"}];
		for(var i=0;i<projectlistTemplate.length;i++){			
			select_template.push({key: projectlistTemplate[i].id , label: projectlistTemplate[i].text});
		}
		select_template=JSON.stringify(select_template);

		//状態選択リスト
		var select_state = '['
			+'{key: "",label: "未処理"},'
			+'{key: "1",label: "作業中"},'
			+'{key: "8",label: "終了"},'
			+'{key: "9",label: "完了"}'
		+']';

    res.render('../views/gantt/gantt', { title: title , role: role, select_categories, select_colors, select_state, select_users, select_template, project_list});
  });
});


router.post("/addfromtemplate", function (req, res) {
	var id = req.body.id;
	var template_id = req.body.template;    
	//var id = req.params.id;
	//var template_id = req.params.template;    
	
	//dataprovider.insertGanttTasks( task ,function (err, result) {
    sendResponse(res, "id:" + id + " template_id:" + template_id); 
  //});
});


router.get("/data", function (req, res) {
  Promise.all([
    dataprovider.getGanttTasks(),
    dataprovider.getGanttLinks()
  ]).then(function(onfulfilled){
    var rows = onfulfilled[0];
    var links = onfulfilled[1];

    for (var i = 0; i < rows.length; i++) {
      rows[i].start_date = rows[i].start_date.format("YYYY-MM-DD");
      rows[i].open = true;
		}		
    res.send({ data: rows, collections: { links: links } });
  });
});


router.post("/data/task", function (req, res) {
  
  var task = getTask(req.body);  
  
	dataprovider.insertGanttTasks( task ,function (err, result) {
		sendResponse(res, "inserted", result ? result.insertId : null, err);
		task.id = result.insertId;
	});
	
	//テンプレートIDが入っているときはテンプレートのタスクも追加
	if(req.body.template){
		Promise.all([
			dataprovider.getProjectTemplateById(req.body.template),
			dataprovider.getGanttTasksTemplateHasParent(req.body.template),
		]).then(function(onfulfilled){
			var templateData = onfulfilled[0];
			var rows = onfulfilled[1];
			for(var i=0;i<rows.length;i++){
				var datediff;
				var startdate = new Date();
				var taskdate = new Date(task.start_date);
				var templatedate = new Date(rows[i].start_date);
				datediff = (templatedate.getTime() - new Date(2000,0,1).getTime() ) /1000/60/60/24;

				startdate.setDate(taskdate.getDate() + datediff);
				var enddate = new Date();
				enddate.setDate(startdate.getDate() + rows[i].duration);

				if(rows[i].parent != 0){
					//子タスクを追加
					var templatetask;
					templatetask = {
						pid: task.pid,
						text: rows[i].text,
						description: rows[i].description,
						category: rows[i].category,
						user: rows[i].user,
						start_date: startdate.date("YYYY-MM-DD"),
						end_dateA: enddate.date("YYYY-MM-DD"),
						duration: rows[i].duration,
						sortorder: rows[i].sortorder,
						progress: rows[i].progress || 0,
						parent: task.id,
						//usergroup: rows[i].usergroup,
						budget: rows[i].budget,
						settlement: rows[i].settlement,
						contract: rows[i].contract,
						customer: rows[i].customer,
						//spreadsheetkey: rows[i].spreadsheetkey,
						color: rows[i].color,
						state: rows[i].state,						
					}

					dataprovider.insertGanttTasks(templatetask, function (err, result) {
						console.log("insertGanttTasks from template:" + JSON.stringify(templatetask));
					});
				}
			}
		})
	}



});

router.put("/data/task/:id", function (req, res) {
	var sid = req.params.id,
		task = getTask(req.body);

  dataprovider.updateGanttTasks(sid, task, 
		function (err, result) {
			sendResponse(res, "updated", null, err);
		});
});

router.delete("/data/task/:id", function (req, res) {
  var sid = req.params.id;
  
	dataprovider.deleteGanttTask(sid,
		function (err, result) {
			sendResponse(res, "deleted", null, err);
		});
});

router.post("/data/link", function (req, res) {
	var link = getLink(req.body);
	dataprovider.insertGanttLinks( link,
		function (err, result) {
			sendResponse(res, "inserted", result ? result.insertId : null, err);
		});
});

router.put("/data/link/:id", function (req, res) {
	var sid = req.params.id,
		link = getLink(req.body);

	dataprovider.updateGanttLinks(sid,link,		
		function (err, result) {
			sendResponse(res, "updated", null, err);
		});
});

router.delete("/data/link/:id", function (req, res) {
	var sid = req.params.id;
	dataprovider.deleteGanttLink(sid,
		function (err, result) {
			sendResponse(res, "deleted", null, err);
		});
});


router.get('/all', checkAuth.isAuthenticated,function(req, res, next) {
  console.log('gantt show: ' + req.session.passport.user.uid);  
  var user_id = req.session.passport.user.id;
  var username = req.session.passport.user.name;
  var role = req.session.passport.user.role;  
  var title = "全体のガントチャート";

	//選択リスト用にデータを取得する
  Promise.all([
    dataprovider.getCategories(),
		dataprovider.getColors(),
		dataprovider.getUsers(),
		dataprovider.getProjectList()
  ]).then(function(onfulfilled){

    var categories = onfulfilled[0];
    var colors = onfulfilled[1];
		var users = onfulfilled[2];
		var projectlist = onfulfilled[3];		
		
		//カテゴリー選択リスト
		var selcategories = [{key:"", label: "カテゴリーを選択..."}];
		for(var i=0;i<categories.length;i++){
			selcategories.push({key: categories[i].id, label: categories[i].categoryname});			
		}
		select_categories=JSON.stringify(selcategories);
		
		//色選択リスト
		var selcolors = [{key:"", label: "色を選択..."}];
		for(var i=0;i<colors.length;i++){
			selcolors.push({key: colors[i].color, label: colors[i].name});			
		}
		select_colors=JSON.stringify(selcolors);

		//ユーザー選択リスト
		var selusers = [];
		for(var i=0;i<users.length;i++){
			selusers.push({key: users[i].id, label: users[i].name});
		}
		select_users=JSON.stringify(selusers);

		//プロジェクト選択リスト
		var project_list = [];
		for(var i=0;i<projectlist.length;i++){			
			project_list.push({value: projectlist[i].id , text: projectlist[i].text});
		}
		project_list=JSON.stringify(project_list);

		//状態選択リスト
		var select_state = '['
			+'{key: "",label: "未処理"},'
			+'{key: "1",label: "作業中"},'
			+'{key: "8",label: "終了"},'
			+'{key: "9",label: "完了"}'
		+']';

    res.render('../views/gantt/ganttA', { title: title , role: role, select_categories, select_colors, select_state, select_users, project_list });
  });
});

function getTask(data) {
	return {
		text: data.text,
		description: data.description,
		category: data.category,
		user: data.user,
		start_date: data.start_date.date("YYYY-MM-DD"),
		end_dateA: data.end_date.date("YYYY-MM-DD"),
		duration: data.duration,
		sortorder: data.sortorder,
		progress: data.progress || 0,
		parent: data.parent,
		//usergroup: data.usergroup,
		budget: data.budget,
		settlement: data.settlement,
		contract: data.contract,
		customer: data.customer,		
		//spreadsheetkey: data.spreadsheetkey,
		color: data.color,
		state: data.state,
		pid: data.pid,   //data.pid,
	};
}



function getLink(data) {
	return {
		source: data.source,
		target: data.target,
		type: data.type
	};
}

function sendResponse(res, action, tid, error) {
	if (error) {
		console.log(error);
		action = "error";
	}

	var result = {
		action: action
	};
	if (tid !== undefined && tid !== null)
		result.tid = tid;

	res.send(result);
}



router.get("/test", function (req, res) {
	res.render('../views/gantt/gantt_test', { });
});



module.exports = router;
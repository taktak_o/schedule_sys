var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var session = require('express-session');
var path = require('path');
var checkAuth = require('./checkAuth');
var dataprovider = require('../dataprovider/dataprovider');
var htmlgenerator = require('../dataprovider/htmlgenerator');

require("date-format-lite");


router.get('/', checkAuth.isAuthenticated,function(req, res, next) {
  console.log('gantt show: ' + req.session.passport.user.uid);  
  var user_id = req.session.passport.user.id;
  var username = req.session.passport.user.name;
  var role = req.session.passport.user.role;  
  var title = "案件テンプレート";

	//選択リスト用にデータを取得する
  Promise.all([
    dataprovider.getCategories(),
		dataprovider.getColors(),
		dataprovider.getUsers(),
		dataprovider.getProjectListTemplates(),
  ]).then(function(onfulfilled){

    var categories = onfulfilled[0];
    var colors = onfulfilled[1];
		var users = onfulfilled[2];
		var projectlist = onfulfilled[3];
		
		//カテゴリー選択リスト
		var selcategories = [{key:"", label: "カテゴリーを選択..."}];
		for(var i=0;i<categories.length;i++){
			selcategories.push({key: categories[i].id, label: categories[i].categoryname});			
		}
		select_categories=JSON.stringify(selcategories);
		
		//色選択リスト
		var selcolors = [{key:"", label: "色を選択..."}];
		for(var i=0;i<colors.length;i++){
			selcolors.push({key: colors[i].color, label: colors[i].name});			
		}
		select_colors=JSON.stringify(selcolors);

		//ユーザー選択リスト
		var selusers = [];
		for(var i=0;i<users.length;i++){
			selusers.push({key: users[i].id, label: users[i].name});
		}
		select_users=JSON.stringify(selusers);

		//プロジェクト選択リスト
		var project_list = [];
		for(var i=0;i<projectlist.length;i++){			
			project_list.push({value: projectlist[i].id , text: projectlist[i].text});
		}
		project_list=JSON.stringify(project_list);

		//状態選択リスト
		var select_state = '['
			+'{key: "",label: "未処理"},'
			+'{key: "1",label: "作業中"},'
			+'{key: "8",label: "終了"},'
			+'{key: "9",label: "完了"}'
		+']';

    res.render('../views/gantt_template/gantt_template', { title: title , role: role, select_categories, select_colors, select_state, select_users, project_list});
  });
});

router.get("/data", function (req, res) {
  Promise.all([
    dataprovider.getGanttTasksTemplate(),
    dataprovider.getGanttLinksTemplate()
  ]).then(function(onfulfilled){

    var rows = onfulfilled[0];
    var links = onfulfilled[1];

    for (var i = 0; i < rows.length; i++) {
      rows[i].start_date = rows[i].start_date.format("YYYY-MM-DD");
      rows[i].open = true;
		}		
    res.send({ data: rows, collections: { links: links } });
  });
});


router.post("/data/task", function (req, res) {
  
  var task = getTask(req.body);  
  
	dataprovider.insertGanttTasksTemplate( task ,function (err, result) {
    sendResponse(res, "inserted", result ? result.insertId : null, err); 
  });
});

router.put("/data/task/:id", function (req, res) {
	var sid = req.params.id,
		task = getTask(req.body);

  dataprovider.updateGanttTasksTemplate(sid, task, 
		function (err, result) {
			sendResponse(res, "updated", null, err);
		});
});

router.delete("/data/task/:id", function (req, res) {
  var sid = req.params.id;
  
	dataprovider.deleteGanttTaskTemplate(sid,
		function (err, result) {
			sendResponse(res, "deleted", null, err);
		});
});

router.post("/data/link", function (req, res) {
	var link = getLink(req.body);
	dataprovider.insertGanttLinksTemplate( link,
		function (err, result) {
			sendResponse(res, "inserted", result ? result.insertId : null, err);
		});
});

router.put("/data/link/:id", function (req, res) {
	var sid = req.params.id,
		link = getLink(req.body);

	dataprovider.updateGanttLinksTemplate(sid,link,		
		function (err, result) {
			sendResponse(res, "updated", null, err);
		});
});

router.delete("/data/link/:id", function (req, res) {
	var sid = req.params.id;
	dataprovider.deleteGanttLinkTemplate(sid,
		function (err, result) {
			sendResponse(res, "deleted", null, err);
		});
});

function getTask(data) {
	return {
		text: data.text,
		description: data.description,
		category: data.category,
		user: data.user,
		start_date: data.start_date.date("YYYY-MM-DD"),
		end_dateA: data.end_date.date("YYYY-MM-DD"),
		duration: data.duration,
		sortorder: data.sortorder,
		progress: data.progress || 0,
		parent: data.parent,
		//usergroup: data.usergroup,
		budget: data.budget,
		settlement: data.settlement,
		contract: data.contract,
		customer: data.customer,		
		//spreadsheetkey: data.spreadsheetkey,
		color: data.color,
		state: data.state,
		pid: data.pid,
	};
}

function getLink(data) {
	return {
		source: data.source,
		target: data.target,
		type: data.type
	};
}

function sendResponse(res, action, tid, error) {
	if (error) {
		console.log(error);
		action = "error";
	}

	var result = {
		action: action
	};
	if (tid !== undefined && tid !== null)
		result.tid = tid;

	res.send(result);
}


module.exports = router;
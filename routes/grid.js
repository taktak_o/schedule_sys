var express = require('express');
var connect = require('connect');
var session = require('express-session');
var flash = require("connect-flash");
var cipherprovider = require('../public/modules/cipherprovider');
var router = express.Router();

require('date-utils').language('jp');

var dataprovider = require('../dataprovider/dataprovider');

router.get('/', function(req, res, next) {
  //console.log('gantt show: ' + req.session.passport.user.uid);  
  var title = "grid test";
  res.render("../views/gridtest/gridtest", { title: title });

});


router.get("/data", function (req, res) {
  Promise.all([
    dataprovider.getGanttTasks(),
    dataprovider.getGanttLinks()
  ]).then(function(onfulfilled){
    var rows = onfulfilled[0];
    var links = onfulfilled[1];

    for (var i = 0; i < rows.length; i++) {
      rows[i].start_date = rows[i].start_date.format("YYYY-MM-DD");
      rows[i].open = true;
		}		
    res.send({ data: rows, collections: { links: links } });
  });
});


router.post("/data", function (req, res) {
  
  var task = getTask(req.body);  
  
	dataprovider.insertGanttTasks( task ,function (err, result) {
    sendResponse(res, "inserted", result ? result.insertId : null, err); 
  });
});

router.put("/data/:id", function (req, res) {
	var sid = req.params.id,
		task = getTask(req.body);

  dataprovider.updateGanttTasks(sid, task, 
		function (err, result) {
			sendResponse(res, "updated", null, err);
		});
});

router.delete("/data/:id", function (req, res) {
  var sid = req.params.id;
  
	dataprovider.deleteGanttTask(sid,
		function (err, result) {
			sendResponse(res, "deleted", null, err);
		});
});



function getTask(data) {
	return {
		text: data.text,
		description: data.description,
		category: data.category,
		user: data.user,
		start_date: data.start_date.date("YYYY-MM-DD"),
		end_dateA: data.end_date.date("YYYY-MM-DD"),
		duration: data.duration,
		sortorder: data.sortorder,
		progress: data.progress || 0,
		parent: data.parent,
		//usergroup: data.usergroup,
		budget: data.budget,
		settlement: data.settlement,
		contract: data.contract,
		customer: data.customer,		
		//spreadsheetkey: data.spreadsheetkey,
		color: data.color,
		state: data.state,
		pid: data.pid,
	};
}


function getLink(data) {
	return {
		source: data.source,
		target: data.target,
		type: data.type
	};
}

function sendResponse(res, action, tid, error) {
	if (error) {
		console.log(error);
		action = "error";
	}

	var result = {
		action: action
	};
	if (tid !== undefined && tid !== null)
		result.tid = tid;

	res.send(result);
}

module.exports = router;
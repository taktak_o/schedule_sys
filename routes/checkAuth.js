var express = require('express');
var connect = require('connect');
var session = require('express-session');
var passport = require("passport");
var cipherprovider = require('../public/modules/cipherprovider');
var router = express.Router();

module.exports.isAuthenticated = function(req, res, next){  
    console.log('checkAuthentication');
    if (req.session.passport !== undefined ) {  // 認証済
    //if (req.isAuthenticated) {  // 認証済
      console.log('isAuthenticated: true');
        next();
    }
    else {  // 認証されていない
        console.log('isAuthenticated: false');
        req.session.originalUrl = req.originalUrl;
        res.redirect('/login');  // ログイン画面に遷移
    }
}
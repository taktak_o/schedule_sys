var session = require('express-session');
var app = require('../app');
var common = require('../bin/common');
var mysql = require('mysql');

var db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: common.getDatabaseName
  });


require("date-format-lite");

//プロジェクト(親タスクが無いもの)
module.exports.getProjectList = function(user_id, state){
  return new Promise(function(resolve, reject){    
    if(!state){state=9};        
    db.query( "select * from gantt_tasks where parent = 0 and state <= ?" , state ,function(err,rows){
      if(err){
        console.log('Error at getProjectList: ' + err);
        reject(err);
      } else {        
        resolve(rows);
      }
    })
  })
};

module.exports.getProjectTaskList = function(user_id, state){
  return new Promise(function(resolve, reject){    
    if(!state){state=9};
    db.query( "select * from project_list where task_state <= ? order by project_start_date,pid,project_title,task_start_date" ,  state ,function(err,rows){
      if(err){
        console.log('Error at getProjectList: ' + err);
        reject(err);
      } else {        
        resolve(rows);
      }
    })
  })
};




//プロジェクト(OLD)
module.exports.getProjects = function(user_id){
  return new Promise(function(resolve, reject){
    //var sqlstring = 'select projects.* from projects INNER JOIN usergroup_data ON projects.usergroup_id = usergroup_data.usergroup_id'
    var sqlstring = 'select * from gantt_tasks where parent=0 and state<8'
//    if(user_id === undefined){
//      sqlstring = sqlstring + ';';
//    } else {
      //sqlstring = sqlstring + ' where usergroup_data.user_id = "' + user_id + '";'
//    }
    db.query( sqlstring , function(err,rows){
      if(err){
        console.log('Error at getProjects: ' + err);
        reject(err);
      } else {
        //console.log('getProjects: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })
  })
};

module.exports.insertProjects = function(projects){
  var date = new Date().date("YYYY/MM/DD hh:mm:ss");
  db.query("insert into projects (pid,color,category_id,project_name,project_description,start_date,end_date,contract_amount,budget_total,customer_id,createdate,editdate ) values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
    [ projects.pid, projects.color, projects.category_id, projects.project_name, projects.project_description, projects.start_date, projects.end_date, projects.contract_amount, projects.budget_total, projects.customer_id, date, date ],
    function (err,result){
      if(err){
        console.log('Error at insertProjects: ' + err);
        return err;
      } else {
        console.log('insertProjects: ' + result.insertId);
        return result;
      }
  });
};



//カテゴリー
module.exports.getCategories = function(){
  return new Promise(function(resolve, reject){
    var sqlstring = 'select * from categories';
    db.query( sqlstring , function(err,rows){
      if(err){        
        console.log('Error at getCategories: ' + err);
        reject(err);
      } else {
        console.log('getCategories: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })  
  })
};

module.exports.getCategoryById = function(id){
  return new Promise(function(resolve,reject){
    db.query('select * from categories where id="' + id + '"', function (err, rows) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        var category;
        if(rows.length != 0){
          category = rows[0];
        }
        console.log(category);
        resolve(category);
      };
    });
  });
};


//カテゴリー削除
module.exports.deleteCategory = function(category_id){  
  db.query('delete from categories where id=' + category_id + ';', function (err, rows) {
    if (err) {
      console.log(err);
      return(err);
    } else {
      console.log(rows);
      return(rows);
    };
  });
};

//カテゴリー更新
module.exports.updateCategory = function( category_id, categoryName){
  var date = new Date().date("YYYY/MM/DD hh:mm:ss");
  db.query("update categories set categoryname = ?,  editdate = ?  where id = ?;",
    [ categoryName, date, category_id], function( err, result ){
      if(err){
        console.log(err);
      } else {
        console.log("update id: " + category_id);
      }
    });
};

//カテゴリー追加
module.exports.insertCategory = function( categoryName ){
  var date = new Date().date("YYYY/MM/DD hh:mm:ss");
  db.query("insert into categories ( categoryname, createdate ) values (?,?);",
    [categoryName, date], function( err, result ){
      console.log("result id: " + result.insertId);
  });
};



//ユーザーグループ
module.exports.getUsergrops = function(user_id){
  return new Promise(function(resolve, reject){
    var sqlstring = 'select * from usergroup INNER JOIN usergroup_data ON usergroup.id = usergroup_data.usergroup_id'
    + ' where usergroup_data.user_id = "' + user_id + '";'
    db.query( sqlstring , function(err,rows){
      if(err){      
        console.log('Error at getUsergrops: ' + err);
        reject(err);
      } else {      
        //console.log('getUsergrops: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })  
  })
};

/* ---------- ユーザーデータ -------------- */
//ユーザーリスト
module.exports.getUsers = function(){
  return new Promise(function(resolve, reject){
    var sqlstring = 'select * from users;'    
    db.query( sqlstring , function(err,rows){
      if(err){
        console.log('Error at getUsers: ' + err);
        reject(err);
      } else {      
        //console.log('getUsers: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })  
  })
};

//ユーザー詳細
module.exports.getUser = function(username){
  return new Promise(function(resolve,reject){
    db.query('select * from users where uid="' + username + '"', function (err, rows) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        var user;
        if(rows.length != 0){
          user = rows[0];  
        }
        console.log(user);
        resolve(user);
      };
    });
  });
};

//ユーザー詳細ById
module.exports.getUserById = function(user_id){
  return new Promise(function(resolve,reject){
    db.query('select * from users where id="' + user_id + '"', function (err, rows) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        var user;
        if(rows.length != 0){
          user = rows[0];
        }
        console.log(user);
        resolve(user);
      };
    });
  });
};

//ユーザー削除
module.exports.deleteUser = function(user_id){  
  db.query('delete from users where id=' + user_id + ';', function (err, rows) {
    if (err) {
      console.log(err);
      return(err);
    } else {
      console.log(rows);
      return(rows);
    };
  });
};

//ユーザー更新
module.exports.updateUser = function( user_id, user){
  var date = new Date().date("YYYY/MM/DD hh:mm:ss");
  db.query("update users set uid = ?, name = ?, email = ?, role = ?, password = ?, editdate = ?  where id = ?;",
    [user.uid, user.name, user.email, user.role, user.password, date, user_id], function( err, result ){
      if(err){
        console.log(err);
      } else {
        console.log("update id: " + user_id);
      }
    });
};

//ユーザー追加
module.exports.insertUser = function( user ){
  var date = new Date().date("YYYY/MM/DD hh:mm:ss");
  db.query("insert into users (uid, name, email, role, password, createdate ) values (?,?,?,?,?,?);",
    [user.uid, user.name, user.email, user.role, user.password, date], function( err, result ){
      console.log("result id: " + result.insertId);
  });
};


//顧客
module.exports.getCustomers = function(){
  return new Promise(function(resolve, reject){
    var sqlstring = 'select * from customers';
    db.query( sqlstring , function(err,rows){
      if(err){        
        console.log('Error at getCustomers: ' + err);
        reject(err);
      } else {
        //console.log('getCustomers: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })  
  })
};

//色
module.exports.getColors = function(){
  return new Promise(function(resolve, reject){
    var sqlstring = 'select * from colors;'    
    db.query( sqlstring , function(err,rows){
      if(err){
        console.log('Error at getColors: ' + err);
        reject(err);
      } else {      
        //console.log('getColors: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })  
  })
};


module.exports.insertColor = function( color ){
  var date = new Date().date("YYYY/MM/DD hh:mm:ss");
  db.query("insert into colors ( color, name ) values (?, ?);",
     [ color.color, color.name], function( err, result ){    
      //console.log("result id: " + result.insertId);
  });
};

module.exports.updateColor = function( color_id, color){
  var date = new Date().date("YYYY/MM/DD hh:mm:ss");
  db.query("update colors set color = ?, name = ? where id = ?;",
    [ color.color, color.name , color_id ], function( err, result ){
      if(err){
        console.log(err);
      } else {
        console.log("update id: " + color_id);
      }
    });
};

module.exports.getColorById = function(color_id){
  return new Promise(function(resolve,reject){
    db.query('select * from colors where id="' + color_id + '"', function (err, rows) {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        var color;
        if(rows.length != 0){
          color = rows[0];
        }
        console.log(color);
        resolve(color);
      };
    });
  });
};

//ユーザー削除
module.exports.deleteColor = function(color_id){  
  db.query('delete from colors where id=' + color_id + ';', function (err, rows) {
    if (err) {
      console.log(err);
      return(err);
    } else {
      console.log(rows);
      return(rows);
    };
  });
};



//スケジュール
module.exports.getEvents = function(user_id, min_date, max_date){
  return new Promise(function(resolve, reject){
    var whereStr = " where user_id=" + user_id;
    if(min_date){
      whereStr += " and end_date >= '" + min_date.date("YYYY-MM-DD hh:mm:ss") + "'";
    }
    if(max_date){
      whereStr += " and end_date <= '" + max_date.date("YYYY-MM-DD hh:mm:ss") + "'";
    }  

    var sqlstring = 'select * from events ' + whereStr + ' order by start_date;';
    db.query( sqlstring , function(err,rows){
      if(err){
        console.log('Error at getEvents: ' + err);
        reject(err);
      } else {      
        //console.log('getEvents: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })  
  })
};

module.exports.updateEvents = function(sid, data, callback){  
  data.start_date = data.start_date.date("YYYY/MM/DD hh:mm:ss");
  data.end_date = data.end_date.date("YYYY/MM/DD hh:mm:ss");
  db.query( 'update events set ? where id = ' + sid + ';', data , function(err,result){
    if(err){
      console.log('Error at updateEvents: ' + err);
      return err;
    } else {      
      //console.log('updateEvents: ' + rows);
      callback(result);
    }
  })  
};


module.exports.insertEvents = function( data, user_id ,callbak){  
  data.user_id = user_id;
  //data.start_date = data.start_date.date("YYYY/MM/DD hh:mm:ss");
  //data.end_date = data.end_date.date("YYYY/MM/DD hh:mm:ss");  
  return db.query("insert into events set ?" , data, function(err,result){
    if(err){
      console.log('Error at insertEvents: ' + err);
      return err;
    } else {
      //console.log('insertEvents: ' + rows );
      //return result;
      callbak(err, result);
    }
  })  
};


module.exports.deleteEvents = function( id , callback) {  
  var sqlstring = 'delete from events '
                + ' where id = ' + id + ';'
  db.query( sqlstring , function(err,result){
    console.log('Error at deleteEvents: ' + err);
    //console.log('deleteEvents: ' + rows);
    callback(err,result);
  })  
};



//ガント
module.exports.getGanttTasks = function(id){
  return new Promise(function(resolve, reject){
    var sqlstring = 'select * from gantt_tasks';
    if(id){
      sqlstring += "where id = " + id;
    }
    db.query( sqlstring , function(err,rows){
      if(err){
        console.log('Error at getGantt: ' + err);
        reject(err);
      } else {      
        //console.log('getGantt: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })  
  })
};

module.exports.getGanttLinks = function(){
  return new Promise(function(resolve, reject){
    var sqlstring = 'select * from gantt_links;';
    db.query( sqlstring , function(err,rows){
      if(err){
        console.log('Error at getGanttLinks: ' + err);
        reject(err);
      } else {      
        //console.log('getGanttLinks: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })  
  })
};

module.exports.updateGanttTasks = function(id,task,callback){  
    db.query( "update gantt_tasks set ? where id ="+id , task, function(err,rows){
    if(err){
      console.log('Error at updateGanttTasks: ' + err);
      return err;
    } else {      
      //console.log('updateEvents: ' + rows);
      return id;
    }
  }) 
  callback(); 
};

module.exports.updateGanttLinks = function(id,link,callback){  
  var sqlstring = 'update gantt_links set '
                + ' source = "' + link.source + '",'
                + ' target = "' + link.target + '",'
                + ' type = "' + link.type + '",'
                + ' where id = ' + id + ';'
  db.query( sqlstring , function(err,rows){
    if(err){
      console.log('Error at updateGanttLinks: ' + err);
      return err;
    } else {
      //console.log('updateGanttLinks: ' + rows);
      return id;
    }
  }) 
  callback(); 
};

module.exports.insertGanttTasks = function( data , callback ){  
  console.log('insertGanttTasks:');
  var sqlstring = 'insert into gantt_tasks ( '
                + ' pid, '                
                + ' text, '
                + ' category, '
                + ' description, '
                + ' start_date ,'
                + ' duration ,'
                + ' end_dateA ,'
                + ' progress ,'
                + ' sortorder ,'
                + ' color ,'
                + ' parent '
                + ' )'
                + ' values ('     
                + '"' + data.pid + '",'
                + '"' + data.text + '",'
                + '"' + data.category + '",'
                + '"' + data.description + '",'
                + '"' + data.start_date + '",'
                + '"' + data.duration + '",'
                + '"' + data.end_dateA + '",'
                + '"' + data.progress + '",'                
                + '"' + data.sortorder + '",'
                + '"' + data.color + '",'                
                + '"' + data.parent + '"' 
                + ');'
  console.log('insertEvents: ' + sqlstring);
  db.query( sqlstring , function(err,result){


  //console.log('insertEvents: ');
  //db.query( "insert into gantt_tasks ? " , data, function(err,result){
    if(err){
      console.log('Error at insertEvents: ' + err);
      callback(err);
    } else { 
        callback(err,result);
    }
  })
};


module.exports.insertGanttLinks = function( link , callback){    
  var sqlstring = 'insert into gantt_links ( '
                + ' source , '
                + ' target ,'                
                + ' type '
                + ' )'
                + ' values ('     
                + '"' + link.source + '",'
                + '"' + link.target + '",'
                + '"' + link.type + '"'
                + ');'
    console.log('insertGanttLinks: ' + sqlstring);
  db.query( sqlstring , function(err,rows){
    if(err){
      console.log('Error at insertGanttLinks: ' + err);
      return err;
    } else {      
      //console.log('insertGanttLinks: ' + rows );
      return rows;
    }
  }) 
  callback(); 
};





module.exports.deleteGanttTask = function(id,callback) {  
  var sqlstring = 'delete from gantt_tasks '
                + ' where id = ' + id + ';'
  db.query( sqlstring , function(err,rows){
    if(err){
      console.log('Error at gantt_tasks: ' + err);
      return err;
    } else {
      //console.log('deleteEvents: ' + rows);
      return id;
    }
  })  
  callback();
};



module.exports.deleteGanttLink = function(id,callback) {  
  var sqlstring = 'delete from gantt_links '
                + ' where id = ' + id + ';'
  db.query( sqlstring , function(err,rows){
    if(err){
      console.log('Error at deleteGanttLink: ' + err);
      return err;
    } else {
      //console.log('deleteEvents: ' + rows);
      return id;
    }
  })  
  callback();
};


//ガントテンプレート
module.exports.getGanttTasksTemplate = function(){
  return new Promise(function(resolve, reject){
    var sqlstring = 'select * from template_gantt_tasks;';
    db.query( sqlstring , function(err,rows){
      if(err){
        console.log('Error at getGanttTasksTemplate: ' + err);
        reject(err);
      } else {      
        //console.log('getGanttTasksTemplate: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })  
  })
};


module.exports.getGanttTasksTemplateHasParent = function(parent){
  return new Promise(function(resolve, reject){
    var sqlstring = 'select * from template_gantt_tasks where parent = ' + parent +';';
    db.query( sqlstring , function(err,rows){
      if(err){
        console.log('Error at getGanttTasksTemplate: ' + err);
        reject(err);
      } else {      
        //console.log('getGanttTasksTemplate: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })  
  })
};



module.exports.getGanttLinksTemplate = function(){
  return new Promise(function(resolve, reject){
    var sqlstring = 'select * from template_gantt_links;';
    db.query( sqlstring , function(err,rows){
      if(err){
        console.log('Error at getGanttLinksTemplate: ' + err);
        reject(err);
      } else {      
        //console.log('getGanttLinksTemplate: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })  
  })
};

module.exports.getGanttLinksTemplateHasParent = function(parent){
  return new Promise(function(resolve, reject){
    var sqlstring = 'select * from template_gantt_links where parent = ' + parent + ';';
    db.query( sqlstring , function(err,rows){
      if(err){
        console.log('Error at getGanttLinksTemplateHasParent: ' + err);
        reject(err);
      } else {      
        //console.log('getGanttLinksTemplate: ' + JSON.stringify(rows));
        resolve(rows);
      }
    })  
  })
};




module.exports.updateGanttTasksTemplate = function(id,task,callback){  
    db.query( "update template_gantt_tasks set ? where id ="+id , task, function(err,rows){
    if(err){
      console.log('Error at updateGanttTasksTemplate: ' + err);
      return err;
    } else {      
      //console.log('updateGanttTasksTemplate: ' + rows);
      return id;
    }
  }) 
  callback(); 
};

module.exports.updateGanttLinksTemplate = function(id,link,callback){  
  var sqlstring = 'update template_gantt_links set '
                + ' source = "' + link.source + '",'
                + ' target = "' + link.target + '",'
                + ' type = "' + link.type + '",'
                + ' where id = ' + id + ';'
  db.query( sqlstring , function(err,rows){
    if(err){
      console.log('Error at updateGanttLinksTemplate: ' + err);
      return err;
    } else {
      //console.log('updateGanttLinksTemplate: ' + rows);
      return id;
    }
  }) 
  callback(); 
};

module.exports.insertGanttTasksTemplate = function( data , callback ){  
  console.log('insertGanttTasksTemplate:');
  var sqlstring = 'insert into template_gantt_tasks ( '
                + ' pid, '                
                + ' text, '
                + ' category, '
                + ' description, '
                + ' start_date ,'
                + ' duration ,'
                + ' end_dateA ,'
                + ' progress ,'
                + ' sortorder ,'
                + ' color ,'
                + ' parent '
                + ' )'
                + ' values ('     
                + '"' + data.pid + '",'
                + '"' + data.text + '",'
                + '"' + data.category + '",'
                + '"' + data.description + '",'
                + '"' + data.start_date + '",'
                + '"' + data.duration + '",'
                + '"' + data.end_dateA + '",'
                + '"' + data.progress + '",'                
                + '"' + data.sortorder + '",'
                + '"' + data.color + '",'                
                + '"' + data.parent + '"' 
                + ');'
  console.log('insertGanttTasksTemplate: ' + sqlstring);
  db.query( sqlstring , function(err,result){


  //console.log('insertGanttTasksTemplate: ');
  
    if(err){
      console.log('Error at insertGanttTasksTemplate: ' + err);
      callback(err);
    } else { 
        callback(err,result);
    }
  })
};


module.exports.insertGanttLinksTemplate = function( link , callback){    
  var sqlstring = 'insert into template_gantt_links ( '
                + ' source , '
                + ' target ,'                
                + ' type '
                + ' )'
                + ' values ('     
                + '"' + link.source + '",'
                + '"' + link.target + '",'
                + '"' + link.type + '"'
                + ');'
    console.log('insertGanttLinksTemplate: ' + sqlstring);
  db.query( sqlstring , function(err,rows){
    if(err){
      console.log('Error at insertGanttLinksTemplate: ' + err);
      return err;
    } else {      
      //console.log('insertGanttLinksTemplate: ' + rows );
      return rows;
    }
  }) 
  callback(); 
};

module.exports.deleteGanttTaskTemplate = function(id,callback) {  
  var sqlstring = 'delete from template_gantt_tasks '
                + ' where id = ' + id + ';'
  db.query( sqlstring , function(err,rows){
    if(err){
      console.log('Error at deleteGanttTaskTemplate: ' + err);
      return err;
    } else {
      //console.log('deleteGanttTaskTemplate: ' + rows);
      return id;
    }
  })  
  callback();
};

module.exports.deleteGanttLinkTemplate = function(id,callback) {  
  var sqlstring = 'delete from template_gantt_links '
                + ' where id = ' + id + ';'
  db.query( sqlstring , function(err,rows){
    if(err){
      console.log('Error at deleteGanttLinkTemplate: ' + err);
      return err;
    } else {
      //console.log('deleteGanttLinkTemplate: ' + rows);
      return id;
    }
  })  
  callback();
};


module.exports.getProjectListTemplates= function(user_id, state){
  return new Promise(function(resolve, reject){    
    if(!state){state=9};        
    db.query( "select * from template_gantt_tasks where parent = 0 and state <= ?" , state ,function(err,rows){
      if(err){
        console.log('Error at getProjectListTemplates: ' + err);
        reject(err);
      } else {        
        resolve(rows);
      }
    })
  })
};



module.exports.getProjectTemplateById= function(id){
  return new Promise(function(resolve, reject){         
    db.query( "select * from template_gantt_tasks where id = ?" , id ,function(err,rows){
      if(err){
        console.log('Error at getProjectTemplateById: ' + err);
        reject(err);
      } else {        
        resolve(rows);
      }
    })
  })
};


//祝日データ
module.exports.Holidays = function(min_date,max_date){
  return new Promise(function(resolve, reject){
    if(!min_date){min_date=0};
    if(!max_date){max_date=new Date(2999,99,99)};
    db.query("select * from holidays where date >=? and date<=?", [min_date,max_date] , function(err,rows){
      if(err){
        console.log('Error at getProjects: ' + err);
        reject(err);
      } else {
        resolve(rows);
      }
    })
  })
};


//顧客
module.exports.getCustomers = function(){
  return new Promise(function(resolve, reject){
    var sqlstring = 'select * from customers;';
    db.query( sqlstring , function(err,rows){
      if(err){
        console.log('Error at getCustomers: ' + err);
        reject(err);
      } else {              
        resolve(rows);
      }
    })  
  })
};

module.exports.updateCustomers = function(id, data, callback){  
  db.query( 'update customers set ? where id = ' + id + ';', data , function(err,result){
    if(err){
      console.log('Error at updateCustomers: ' + err);
      return err;
    } else {            
      callback(result);
    }
  })  
};


module.exports.insertCustomers = function( data ,callbak){  
  return db.query("insert into customers set ?" , data, function(err,result){
    if(err){
      console.log('Error at insertCustomers: ' + err);
      return err;
    } else {
      callbak(err, result);
    }
  })  
};


module.exports.deleteCustomers = function( id , callback) {  
  var sqlstring = 'delete from customers '
                + ' where id = ' + id + ';'
  db.query( sqlstring , function(err,result){
    console.log('Error at deleteCustomers: ' + err);
    callback(err,result);
  })  
};


var crypto = require("crypto");

var keyString = 'norad_sys';

module.exports.getCipher = function(target){
    if(!target){return null;}

    var cipher = crypto.createCipher('aes192', keyString);
    cipher.update(target, 'utf8', 'hex');
    var cipheredText = cipher.final('hex');
    return cipheredText;
}
/*
    var sha = crypto.createHmac("sha256", "norad_sys");
    sha.update(target);
    return sha.digest("hex");
};
*/

module.exports.getDeCipher = function(target){
    if(!target){return null;}
    var decipher = crypto.createDecipher('aes192', keyString);
    var dec = decipher.update(target, 'hex', 'utf8');
    dec = decipher.final('utf8');
    return dec;
};
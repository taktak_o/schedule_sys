-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: nsms
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryname` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id'categories'_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'default','2017-10-10 00:00:00','2017-10-10 00:00:00');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
  `default` varchar(8) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'#ff0000','赤',NULL),(2,'#0000ff','青','1'),(3,'#42f477','緑',NULL),(4,'#fff020','黄',NULL),(5,'#f0730e','橙',NULL);
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `s_name` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `zip_code` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `address1` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `address2` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `fax` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `person` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `sir` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `creaetdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `text` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `creaetdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  `cal_id` int(11) DEFAULT '0',
  `details` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `rec_type` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `event_length` int(11) DEFAULT NULL,
  `event_pid` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `color` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `textColor` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_plan`
--

DROP TABLE IF EXISTS `events_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `text` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `creaetdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  `cal_id` int(11) DEFAULT '0',
  `details` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `rec_type` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `event_length` int(11) DEFAULT NULL,
  `event_pid` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `color` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `textColor` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_plan`
--

LOCK TABLES `events_plan` WRITE;
/*!40000 ALTER TABLE `events_plan` DISABLE KEYS */;
/*!40000 ALTER TABLE `events_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gantt_links`
--

DROP TABLE IF EXISTS `gantt_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gantt_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` int(11) DEFAULT NULL,
  `target` int(11) DEFAULT NULL,
  `type` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
  `color` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gantt_links`
--

LOCK TABLES `gantt_links` WRITE;
/*!40000 ALTER TABLE `gantt_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `gantt_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gantt_tasks`
--

DROP TABLE IF EXISTS `gantt_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gantt_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(120) COLLATE utf8mb4_bin DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `end_dateA` datetime DEFAULT NULL,
  `sortorder` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `progress` float DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `color` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `user` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
  `usergroup` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `category` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `budget` float DEFAULT NULL,
  `settlement` float DEFAULT NULL,
  `contract` float DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `customer` varchar(120) COLLATE utf8mb4_bin DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `spreadsheetkey` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  `pid` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `textColor` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `progressColor` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gantt_tasks`
--

LOCK TABLES `gantt_tasks` WRITE;
/*!40000 ALTER TABLE `gantt_tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `gantt_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `holidays`
--

DROP TABLE IF EXISTS `holidays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idholidays_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `holidays`
--

LOCK TABLES `holidays` WRITE;
/*!40000 ALTER TABLE `holidays` DISABLE KEYS */;
INSERT INTO `holidays` VALUES (1,'元日','2016-01-01 00:00:00'),(2,'成人の日','2016-01-11 00:00:00'),(3,'建国記念の日','2016-02-11 00:00:00'),(4,'春分の日','2016-03-20 00:00:00'),(5,'昭和の日','2016-04-29 00:00:00'),(6,'憲法記念日','2016-05-03 00:00:00'),(7,'みどりの日','2016-05-04 00:00:00'),(8,'こどもの日','2016-05-05 00:00:00'),(9,'海の日','2016-07-18 00:00:00'),(10,'山の日','2016-08-11 00:00:00'),(11,'敬老の日','2016-09-19 00:00:00'),(12,'秋分の日','2016-09-22 00:00:00'),(13,'体育の日','2016-10-10 00:00:00'),(14,'文化の日','2016-11-03 00:00:00'),(15,'勤労感謝の日','2016-11-23 00:00:00'),(16,'天皇誕生日','2016-12-23 00:00:00'),(17,'元日','2017-01-01 00:00:00'),(18,'成人の日','2017-01-09 00:00:00'),(19,'建国記念の日','2017-02-11 00:00:00'),(20,'春分の日','2017-03-20 00:00:00'),(21,'昭和の日','2017-04-29 00:00:00'),(22,'憲法記念日','2017-05-03 00:00:00'),(23,'みどりの日','2017-05-04 00:00:00'),(24,'こどもの日','2017-05-05 00:00:00'),(25,'海の日','2017-07-17 00:00:00'),(26,'山の日','2017-08-11 00:00:00'),(27,'敬老の日','2017-09-18 00:00:00'),(28,'秋分の日','2017-09-23 00:00:00'),(29,'体育の日','2017-10-09 00:00:00'),(30,'文化の日','2017-11-03 00:00:00'),(31,'勤労感謝の日','2017-11-23 00:00:00'),(32,'天皇誕生日','2017-12-23 00:00:00'),(33,'元日','2018-01-01 00:00:00'),(34,'成人の日','2018-01-08 00:00:00'),(35,'建国記念の日','2018-02-11 00:00:00'),(36,'春分の日','2018-03-21 00:00:00'),(37,'昭和の日','2018-04-29 00:00:00'),(38,'憲法記念日','2018-05-03 00:00:00'),(39,'みどりの日','2018-05-04 00:00:00'),(40,'こどもの日','2018-05-05 00:00:00'),(41,'海の日','2018-07-16 00:00:00'),(42,'山の日','2018-08-11 00:00:00'),(43,'敬老の日','2018-09-17 00:00:00'),(44,'秋分の日','2018-09-23 00:00:00'),(45,'体育の日','2018-10-08 00:00:00'),(46,'文化の日','2018-11-03 00:00:00'),(47,'勤労感謝の日','2018-11-23 00:00:00'),(48,'天皇誕生日','2018-12-23 00:00:00');
/*!40000 ALTER TABLE `holidays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `project_list`
--

DROP TABLE IF EXISTS `project_list`;
/*!50001 DROP VIEW IF EXISTS `project_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `project_list` AS SELECT 
 1 AS `id`,
 1 AS `pid`,
 1 AS `project_start_date`,
 1 AS `project_color`,
 1 AS `project_title`,
 1 AS `project_state`,
 1 AS `task_id`,
 1 AS `task_sortorder`,
 1 AS `task_color`,
 1 AS `task_title`,
 1 AS `task_start_date`,
 1 AS `task_end_dateA`,
 1 AS `task_state`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `color` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `categoryname` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
  `project_name` varchar(120) COLLATE utf8mb4_bin DEFAULT NULL,
  `project_description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `owner_name` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
  `usergroup_id` int(11) DEFAULT NULL,
  `usergroup_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `contract_amount` float DEFAULT NULL,
  `budget_total` float DEFAULT NULL,
  `settlement_total` float DEFAULT NULL,
  `spreadsheetkey` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_name` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_init`
--

DROP TABLE IF EXISTS `system_init`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_init` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `preference` varchar(120) COLLATE utf8mb4_bin DEFAULT NULL,
  `argument` varchar(120) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_init`
--

LOCK TABLES `system_init` WRITE;
/*!40000 ALTER TABLE `system_init` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_init` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `task_name` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `task_description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  `estimated_time` float DEFAULT NULL,
  `spreadsheetkey` varchar(24) CHARACTER SET utf8mb4 DEFAULT NULL,
  `actual_time` float DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `owner_name` varchar(24) CHARACTER SET utf8mb4 DEFAULT NULL,
  `budget` float DEFAULT NULL,
  `settlement` float DEFAULT NULL,
  `usergroup_name` varchar(24) CHARACTER SET utf8mb4 DEFAULT NULL,
  `usergroup_id` int(11) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_gantt_links`
--

DROP TABLE IF EXISTS `template_gantt_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_gantt_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` int(11) DEFAULT NULL,
  `target` int(11) DEFAULT NULL,
  `type` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL,
  `color` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_gantt_links`
--

LOCK TABLES `template_gantt_links` WRITE;
/*!40000 ALTER TABLE `template_gantt_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_gantt_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_gantt_tasks`
--

DROP TABLE IF EXISTS `template_gantt_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_gantt_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(120) COLLATE utf8mb4_bin DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `end_dateA` datetime DEFAULT NULL,
  `sortorder` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `progress` float DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `color` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `user` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
  `usergroup` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `category` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `budget` float DEFAULT NULL,
  `settlement` float DEFAULT NULL,
  `contract` float DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `customer` varchar(120) COLLATE utf8mb4_bin DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `spreadsheetkey` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  `pid` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `textColor` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  `progressColor` varchar(12) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_gantt_tasks`
--

LOCK TABLES `template_gantt_tasks` WRITE;
/*!40000 ALTER TABLE `template_gantt_tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_gantt_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_tasks`
--

DROP TABLE IF EXISTS `template_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) DEFAULT NULL,
  `task_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `task_description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  `estimated_time` float DEFAULT NULL,
  `actual_time` float DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `owner_name` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
  `budget` float DEFAULT NULL,
  `settlement` float DEFAULT NULL,
  `usergroup_name` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
  `usergroup_id` int(11) DEFAULT NULL,
  `spreadsheetkey` varchar(24) COLLATE utf8mb4_bin DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_tasks`
--

LOCK TABLES `template_tasks` WRITE;
/*!40000 ALTER TABLE `template_tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tamplate_name` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `spreadsheetkey` varchar(24) CHARACTER SET utf8mb4 DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates`
--

LOCK TABLES `templates` WRITE;
/*!40000 ALTER TABLE `templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usergroup`
--

DROP TABLE IF EXISTS `usergroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usergroupname` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usergroup`
--

LOCK TABLES `usergroup` WRITE;
/*!40000 ALTER TABLE `usergroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `usergroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usergroup_data`
--

DROP TABLE IF EXISTS `usergroup_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroup_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `usergroup_id` int(11) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usergroup_data`
--

LOCK TABLES `usergroup_data` WRITE;
/*!40000 ALTER TABLE `usergroup_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `usergroup_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `name` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `email` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `password` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `editdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_UNIQUE` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin',9,'管理者','','3ff58ca381ed964a526d670fbb1b6fd0',NULL,'2017-10-23 01:37:18');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `project_list`
--

/*!50001 DROP VIEW IF EXISTS `project_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `project_list` AS select `projects`.`id` AS `id`,`projects`.`pid` AS `pid`,`projects`.`start_date` AS `project_start_date`,`projects`.`color` AS `project_color`,`projects`.`text` AS `project_title`,`projects`.`state` AS `project_state`,`tasks`.`id` AS `task_id`,`tasks`.`sortorder` AS `task_sortorder`,`tasks`.`color` AS `task_color`,`tasks`.`text` AS `task_title`,`tasks`.`start_date` AS `task_start_date`,`tasks`.`end_dateA` AS `task_end_dateA`,`tasks`.`state` AS `task_state` from (`gantt_tasks` `projects` left join `gantt_tasks` `tasks` on((`projects`.`id` = `tasks`.`parent`))) order by `projects`.`start_date`,`projects`.`pid`,`projects`.`text`,`tasks`.`start_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-06 22:40:01
